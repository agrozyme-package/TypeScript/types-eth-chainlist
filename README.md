# types-eth-chainlist

TypeScript definitions for [eth-chainlist](https://www.npmjs.com/package/eth-chainlist)

- Use this package, to create folder `@types/eth-chainlist` under the source folder and put the code in `index.d.ts`

```ts
declare module 'eth-chainlist' {
  export * from '@agrozyme/types-eth-chainlist';
}
```
