import { Chain } from 'eth-chains';

export declare function rawChainData(): Chain[];

export declare function getChainById(chainId: number): Chain | undefined;

export declare function getChainByNetworkId(networkId: number): Chain | undefined;

export declare function getChainByName(name: string): Chain | undefined;

export declare function getChainByShortName(shortName: string): Chain | undefined;
